#use wml::debian::translation-check translation="282fe47153ee7ae459dbd068bec0e572c214acb8" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В wpa_supplicant и hostapd было обнаружено несколько уязвимостей.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12695">CVE-2020-12695</a>

    <p>Было обнаружено, что hostapd при определённых условиях неправильно обрабатывает
    сообщения подписки UPnP, позволяя злоумышленнику вызывать отказ
    в обслуживании.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-0326">CVE-2021-0326</a>

    <p>Было обнаружено, что wpa_supplicant неправильно обрабатывает групповую информацию
    P2P (Wi-Fi Direct) от активных владельцев группы. Злоумышленник, находящийся в области
    распространения сигнала устройства, на котором запущена служба P2P, может
    использовать эту уязвимость для вызова отказа в обслуживании или выполнения
    произвольного кода.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27803">CVE-2021-27803</a>

    <p>Было обнаружено, что wpa_supplicant неправильно обрабатывает запросы по
    обнаружению устройств P2P (Wi-Fi Direct). Злоумышленник, находящийся в области
    действия распространения сигнала устройства, на котором запущена служба P2P, может
    использовать эту уязвимость для вызова отказа в обслуживании или выполнения
    произвольного кода.</p></li>

</ul>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 2:2.7+git20190128+0c1e29f-6+deb10u3.</p>

<p>Рекомендуется обновить пакеты wpa.</p>

<p>С подробным статусом поддержки безопасности wpa можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/wpa">\
https://security-tracker.debian.org/tracker/wpa</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4898.data"
