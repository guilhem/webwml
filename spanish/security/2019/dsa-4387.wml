#use wml::debian::translation-check translation="19fdc288616ee3bfe6ee122b16cd10940121ffb2"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Harry Sintonen, de F-Secure Corporation, descubrió múltiples vulnerabilidades en
OpenSSH, una implementación de la colección de protocolos SSH. Todas las vulnerabilidades
han sido encontradas en el cliente scp, que implementa el protocolo SCP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20685">CVE-2018-20685</a>

    <p>Debido a una validación incorrecta de los nombres de directorios, el cliente scp permite que los servidores
    modifiquen permisos del directorio destino utilizando como nombre de directorio el nombre vacío
    o un punto.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6109">CVE-2019-6109</a>

    <p>Debido a la ausencia de codificación de caracteres en la visualización del progreso de la operación en curso, se puede usar
    el nombre de objeto para manipular la salida del cliente. Por ejemplo, se podrían usar códigos
    ANSI para ocultar la transferencia de ficheros adicionales.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6111">CVE-2019-6111</a>

    <p>Debido a una validación insuficiente por parte del cliente scp de los nombres de ruta enviados por
    el servidor, un servidor malicioso puede sobrescribir ficheros arbitrarios en el directorio
    de destino. Si se incluye la opción recursiva (-r), el servidor puede manipular
    también subdirectorios.</p>

    <p>La comprobación añadida en esta versión puede dar lugar a regresión si el cliente y
    el servidor tienen diferencias en las reglas de expansión de caracteres comodín. Si se confía en el
    servidor a estos efectos, se puede desactivar la comprobación con una nueva opción -T del
    cliente scp.</p></li>

</ul>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 1:7.4p1-10+deb9u5.</p>

<p>Le recomendamos que actualice los paquetes de openssh.</p>

<p>Para información detallada sobre el estado de seguridad de openssh, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/openssh">\
https://security-tracker.debian.org/tracker/openssh</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4387.data"
