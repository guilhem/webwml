#use wml::debian::template title="Informazioni per consulenti Debian" MAINPAGE="true"
#use wml::debian::translation-check translation="5f70081cb38037d61262d8cb159fb510c9315981" maintainer="Giuseppe Sacco"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#policy">Regolamento per la pagina dei consulenti Debian</a></li>
  <li><a href="#change">Aggiunte, modifiche e cancellazione dei consulenti</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>I consulenti possono comunicare con altri consulenti tramite la
<a href="https://lists.debian.org/debian-consultants/">lista di messaggi debian-consultants</a>.
La lista non è moderata: tutti possono scrivervi. La lista ha anche un archivio pubblico.</p>
</aside>

<h2><a id="policy">Regolamento per la pagina dei consulenti Debian</a></h2>

<p>Per essere aggiunti all'elenco dei consulenti del sito web Debian devono
essere seguite le seguenti regole:</p>

<ul>
  <li><strong>Informazioni obbligatorie:</strong><br />
  È necessario fornire un indirizzo email funzionante e rispondere a messaggi
  entro al massimo quattro settimane. Per bloccare eventuali abusi, tutte le richieste
  (di inserimento, rimozione, aggiornamento) devono essere inviate dallo stesso
  indirizzo. Per evitare lo spam, è possibile chiedere che
  l'indirizzo email sia camuffato (per esempio <em>debian -dot- consulting -at-
  example -dot- com</em>). In questo caso la richiesta va fatta esplicitamente
  quando si chiede l'inserimento. In alternativa si può chiedere che l'indirizzo
  email non appaia per nulla sul sito web (ma in ogni caso è necessario per la
  gestione dell'elenco).
  Se si vuole che l'indirizzo non sia inserito nell'elenco, si può fornire un
  URL di un modulo di contatto sul proprio sito web. <q>Contact</q> è proprio per
  questo.
  </li>
  <li><strong>Il sito web:</strong><br />
	Se si fornisce un collegamento ad un sito web, quel sito deve menzionare
	l'attività di consulenza su Debian. È molto apprezzato, anche
	se non obbligatorio, fornire il collegamento diretto al posto di quello alla
	pagina principale del sito. Ma solo se l'informazione è comunque
	ragionevolmente accessibile.
  </li>
  <li><strong>Più città/regioni/nazioni:</strong><br />
	Ciascun consulente ha la possibilità di scegliere in quale nazione (una
	soltanto) voglia essere elencato. Si può inserire un solo indirizzo
	da pubblicare ma ovviamente si possono inserire altre città o
	nazioni nelle informazioni addizionali o nel proprio sito web.
  </li>
  <li><strong>Regole per sviluppatori Debian:</strong><br />
        Non è permesso utilizzare l'indirizzo ufficiale <em>@debian.org</em>
	per l'elenco dei consulenti. Lo stesso vale per il sito web:
	non si può usare un <em>dominio *.debian.org</em> ufficiale, come descritto nella
	<a href="$(HOME)/devel/dmup">DMUP</a> (Debian Machine Usage Policy).
  </li>
</ul>

<p>Se le regole sopra elencate non sono soddisfatte, il consulente
riceverà un email di notifica riguardo la rimozione dall'elenco a meno
che non torni a soddisfare i criteri entro un periodo di quattro settimane.</p>

<p>Le informazioni possono essere parzialmente (o totalmente) rimosse se non
soddisfano più il regolamento, o comunque a discrezione dei gestori
dell'elenco.</p>

<h2><a id="change">Aggiunte, modifiche e cancellazione dei consulenti</a></h2>

<p>Se si vuole che la propria società sia inserita in queste pagine, si
deve mandare una email all'indirizzo <a
href="mailto:consultants@debian.org">consultants@debian.org</a>, in inglese,
con le informazioni tra quelle sotto specificate che volete che siano
mostrate (l'indirizzo email è obbligatorio, tutto il resto è facoltativo e
a vostra discrezione):</p>

<ul>
  <li>Country for which you want to be listed (Stato per il quale si vuole
     essere inseriti)</li>
  <li>Name (Nome)</li>
  <li>Company (Società)</li>
  <li>Address (Indirizzo)</li>
  <li>Phone (Telefono)</li>
  <li>Fax</li>
  <li>Contact (Contatto)</li>
  <li>E-mail</li>
  <li>URL</li>
  <li>Rates (Tariffe)</li>
  <li>Additional information (Altre informazioni)</li>
</ul>

<p>Una richiesta di aggiornamento deve essere inviata a 
<a href="mailto:consultants@debian.org">consultants@debian.org</a>,
preferibilmente dallo stesso indirizzo email pubblicato nella pagina dei
consulenti (<a href="https://www.debian.org/consultants/">\
https://www.debian.org/consultants/</a>).</p>

