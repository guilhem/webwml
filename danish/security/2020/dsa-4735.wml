#use wml::debian::translation-check translation="f316e0ce25840c6590881cb5b3cec62cc137c07d" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i bootloader'en GRUB2.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10713">CVE-2020-10713</a>

    <p>Der blev fundet en i koden til fortolkning af grub.cfg, hvilken gjorde 
    det muligt at bryde UEFI Secure Boot og indlæse vilkårlig kode.  Flere 
    oplysninger finder man i: 
    <a href="https://www.eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/">\
    https://www.eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14308">CVE-2020-14308</a>

    <p>Man opdagede at grub_malloc ikke validerede størrelsesallokeringen, 
    hvilket muliggjorde et aritmetisk overløb og efterfølgende et heapbaseret
    bufferoverløb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14309">CVE-2020-14309</a>

    <p>Et heltalsoverløb i grub_squash_read_symlink kunne føre til et 
    headbaseret bufferoverløb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14310">CVE-2020-14310</a>

    <p>Et heltalsoverløb i read_section_from_string kunne føre til et 
    heapbaseret bufferoverløb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14311">CVE-2020-14311</a>

    <p>Et heltalsoverløb i grub_ext2_read_link kunne føre til et heapbaseret 
    bufferoverløb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15706">CVE-2020-15706</a>

    <p>script: Undgå en anvendelse efter frigivelse, når en funktion redefineres 
    under udførslen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15707">CVE-2020-15707</a>

    <p>En heltalsoverløbsfejl blev fundet i initrd's 
    størrelseshåndtering.</p></li>

</ul>

<p>Flere oplysninger findes i: 
<a href="https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot">\
https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot</a></p>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 2.02+dfsg1-20+deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine grub2-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende grub2, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/grub2">\
https://security-tracker.debian.org/tracker/grub2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4735.data"
