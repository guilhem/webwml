#use wml::debian::translation-check translation="f9b3401db5e222ddd741acdd79fc20ed59ed8961" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sikkerhedssårbarheder er fundet i libpgjava, den officielle PostgreSQL 
JDBC-driver.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13692">CVE-2020-13692</a>

    <p>En XML External Entity-svaghed (XXE) blev fundet i PostgreSQL 
    JDBC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21724">CVE-2022-21724</a>

    <p>JDBC-driveren kontrollerede ikke om visse klasser implementerede det 
    forventede interface, før klassen blev instantieret.  Det kunne føre til 
    udførelse af kode gennem vilkårlige klasser.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26520">CVE-2022-26520</a>

    <p>En angriber (som kontrollerer jdbc-URLen eller -egenskaber) kunne kalde 
    java.util.logging.FileHandler til at skrive til vilkårlige filer gennem 
    loggerFile- og loggerLevel-forbindelsesegenkaber.</p></li>

</ul>

<p>I den gamle stabile distribution (buster), er disse problemer rettet
i version 42.2.5-2+deb10u1.</p>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 42.2.15-1+deb11u1.</p>

<p>Vi anbefaler at du opgraderer dine libpgjava-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende libpgjava, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/libpgjava">\
https://security-tracker.debian.org/tracker/libpgjava</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5196.data"
