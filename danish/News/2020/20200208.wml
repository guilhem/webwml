#use wml::debian::translation-check translation="96c05360e385187167f1ccbce37d38ce2e5e6920"
<define-tag pagetitle>Opdateret Debian 10: 10.3 udgivet</define-tag>
<define-tag release_date>2020-02-08</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.3</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debian-projektet er stolt over at kunne annoncere den tredje opdatering af 
dets stabile distribution, Debian <release> (kodenavn <q><codename></q>).
Denne opdatering indeholder primært rettelser af sikkerhedsproblemer i den 
stabile udgave, sammen med nogle få rettelser af alvorlige problemer.  
Sikkerhedsbulletiner er allerede udgivet separat og der vil blive refereret til 
dem, hvor de er tilgængelige.</p>

<p>Bemærk at denne opdatering ikke er en ny udgave af Debian GNU/Linux
<release>, den indeholder blot opdateringer af nogle af de medfølgende pakker.  
Der er ingen grund til at smide gamle <q><codename></q>-medier væk.  Efter en 
installering, kan pakkerne opgradere til de aktuelle versioner ved hjælp af et 
ajourført Debian-filspejl.</p>

<p>Dem der hyppigt opdaterer fra security.debian.org, behøver ikke at opdatere 
ret mange pakker, og de fleste opdateringer fra security.debian.org er indeholdt 
i denne opdatering.</p>

<p>Nye installeringsfilaftryk vil snart være tilgængelige fra de sædvanlige 
steder.</p>

<p>Opdatering af en eksisterende installation til denne revision, kan gøres ved 
at lade pakkehåndteringssystemet pege på et af Debians mange HTTP-filspejle. En 
omfattende liste over filspejle er tilgængelig på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Forskellige fejlrettelser</h2>

<p>Denne opdatering til den stabile udgave tilføjer nogle få vigtige rettelser 
til følgende pakker:</p>

<table border=0>
<tr><th>Pakke</th>					<th>Årsag</th></tr>
<correction alot 					"Fjerner udløbstid fra testsuitenøgler, retter opbygningsfejl">
<correction atril 					"Retter segmenteringsfejl når ingen dokumenter er indlæst; retter læsning af uinitialiseret hukommelse [CVE-2019-11459]">
<correction base-files 					"Opdaterer til denne punktudgivelse">
<correction beagle 					"Leverer wrapperskript i stedet for symlinks til JARs, så de igen fungerer">
<correction bgpdump 					"Retter segmenteringsfejl">
<correction boost1.67 					"Retter udefineret virkemåde førende til at libboost-numpy gik ned">
<correction brightd 					"Sammenligner faktisk væriden læst i /sys/class/power_supply/AC/online med <q>0</q>">
<correction casacore-data-jplde 			"Medtager tabeller op til 2040">
<correction clamav 					"Ny opstrømsudgave; retter problem med lammelsesangreb [CVE-2019-15961]; fjerner valgmuligheden ScanOnAccess, som erstattes med clamonacc">
<correction compactheader 				"Ny opstrømsudgave kompatibel med Thunderbird 68">
<correction console-common 				"Retter regression som førte til at filer ikke kunne indlæses">
<correction csh 					"Retter segmenteringsfejl ved eval">
<correction cups 					"Retter hukommelseslækage i ppdOpen; retter validering af standardsprog i ippSetValuetag [CVE-2019-2228]">
<correction cyrus-imapd 				"Tilføjer BACKUP-type til cyrus-upgrade-db, hvilket løser opgraderingsproblemer">
<correction debian-edu-config 				"Bevarer proxyindstillinger i klient hvis WPAD ikke kan tilgås">
<correction debian-installer 				"Genopbygget mod proposed-updates; tilpasser generering af mini.iso på arm så EFI-netboot fungerer; opdaterer USE_UDEBS_FROM's standard fra unstable til buster, for at hjælpe brugere med at foretage lokale opbygninger">
<correction debian-installer-netboot-images 		"Genopbygget mod proposed-updates">
<correction debian-security-support 			"Opdaterer flere pakkers sikkerhedssupportstatus">
<correction debos 					"Genopbygget mod opdateretgolang-github-go-debos-fakemachine">
<correction dispmua 					"Ny opstrømsudgave kompatibel med Thunderbird 68">
<correction dkimpy 					"Ny stabil opstrømsudgave">
<correction dkimpy-milter 				"Retter rettighedshåndtering ved start så Unix-sockets fungerer">
<correction dpdk 					"Ny stabil opstrømsudgave">
<correction e2fsprogs 					"Retter potentielt stakunderløb i e2fsck [CVE-2019-5188]; retter anvendelse efter frigivelse i e2fsck">
<correction fig2dev 					"Tillader at Fig v2-tekststrenge kan slutte med flere ^A [CVE-2019-19555]; afviser enorme arrow-typer som medfører heltalsoverløb [CVE-2019-19746]; retter flere nedbrud [CVE-2019-19797]">
<correction freerdp2 					"Retter returnhåndtering ved realloc [CVE-2019-17177]">
<correction freetds 					"tds: Sikrer at UDT har varint opsat til 8 [CVE-2019-13508]">
<correction git-lfs 					"Retter opbygningsproblemer med nyere versioner af Go">
<correction gnubg 					"Forøger størrelsen på statiske buffere som anvendes til opbygningsmedddelelser under programstart, så den spanske oversættelse ikke får bufferen til at løbe over">
<correction gnutls28 					"Retter interop-problem med gnutls 2.x; retter fortolkning af certifikater som anvender RegisteredID">
<correction gtk2-engines-murrine 			"Retter mulighed for samtidig installering med andre temaer">
<correction guile-2.2 					"Retter opbygningsfejl">
<correction libburn 					"Retter <q>cdrskin-flersporsbrænding var langsom og gik i stå efter spor 1</q>">
<correction libcgns 					"Retter opbygningsfejl på ppc64el">
<correction libimobiledevice 				"Håndterer på korrekt vis delviste SSL-skrivninger">
<correction libmatroska 				"Forøger afhængighed af delt bibliotek til 1.4.7, da den version indførte nye symboler">
<correction libmysofa 					"Sikkerhedsrettelser [CVE-2019-16091 CVE-2019-16092 CVE-2019-16093 CVE-2019-16094 CVE-2019-16095]">
<correction libole-storage-lite-perl 			"Retter fortolkning af år fra 2020 og frem">
<correction libparse-win32registry-perl			"Retter fortolkning af år fra 2020 og frem">
<correction libperl4-corelibs-perl 			"Retter fortolkning af år fra 2020 og frem">
<correction libsolv 					"Retter heapbufferoverløb [CVE-2019-20387]">
<correction libspreadsheet-wright-perl 			"Retter tidligere ubrugelige OpenDocument-regneark og overførsel af JSON-formateringsvalgmuligheder">
<correction libtimedate-perl 				"Retter fortolkning af år fra 2020 og frem">
<correction libvirt 					"Apparmor: Tillad at man kører pygrub; gør ikke osxsave, ospke til en QEMU-kommandolinje; det hjælper nyere QEMU med nogle opsætninger genereret af virt-install">
<correction libvncserver 				"RFBserver: Læk ikke stakhukommelse til remote [CVE-2019-15681]; løser en frysning under lukning af forbindelse og en segmenteringsfejl ved flertrådede VNC-servere; retter problem med forbindelse til VMWare-servere; retter nedbrud i x11vnc når vncviewer forbinder sig">
<correction limnoria 					"Retter fjernafsløring af information samt muligvis fjernudførelse af kode i Math-plugin'en [CVE-2019-19010]">
<correction linux 					"Ny stabil opstrømsudgave">
<correction linux-latest 				"Opdaterer til Linux' 4.19.0-8 kerne-ABI">
<correction linux-signed-amd64 				"Ny stabil opstrømsudgave">
<correction linux-signed-arm64 				"Ny stabil opstrømsudgave">
<correction linux-signed-i386 				"Ny stabil opstrømsudgave">
<correction mariadb-10.3 				"Ny stabil opstrømsudgave [CVE-2019-2938 CVE-2019-2974 CVE-2020-2574]">
<correction mesa 					"Kalder shmget() med rettigheden 0600 i stedet for 0777 [CVE-2019-5068]">
<correction mnemosyne 					"Tilføjer manglende afhængighed af PIL">
<correction modsecurity 				"Retter fejl i forbindelse med fortolkning af cookieheader [CVE-2019-19886]">
<correction node-handlebars 				"Tillad ikke direkte kald af <q>helperMissing</q> og <q>blockHelperMissing</q> [CVE-2019-19919]">
<correction node-kind-of 				"Retter typekontrolsårbarhed i ctorName() [CVE-2019-20149]">
<correction ntpsec 					"Retter langsomme DNS-forekomster; retter ntpdate -s (syslog) for at rette hook'et if-up; dokumentationsrettelser">
<correction numix-gtk-theme 				"Retter mulighed for samtidig installering med andre temaer">
<correction nvidia-graphics-drivers-legacy-340xx 	"Ny stabil opstrømsudgave">
<correction nyancat 					"Genopbygger i et rent miljø for at tilføje systemd-unit'et for nyancat-server">
<correction openjpeg2 					"Retter heapoverløb [CVE-2018-21010] og heltalsoverløb [CVE-2018-20847]">
<correction opensmtpd 					"Advarer brugere om ændring af smtpd.conf's syntaks (i tidligere versioner); installerer smtpctl setgid opensmtpq; håndterer exitkode som ikke er nul fra hostname i opsætningsfasen">
<correction openssh 					"Afvis (ikke-fatalt) ipc i seccomp-sandkassen, retter fejl med OpenSSL 1.1.1d og Linux &lt; 3.19 på nogle arkitekturer">
<correction php-horde 					"Retter gemt problem med udførelse af skripter på tværs af servere i Horde Cloud Block [CVE-2019-12095]">
<correction php-horde-text-filter 			"Retter ugyldige regulære udtræk">
<correction postfix 					"Ny stabil opstrømsudgave">
<correction postgresql-11 				"Ny stabil opstrømsudgave">
<correction print-manager 				"Retter nedbrud hvis CUPS returnerer den samme id for flere printjob">
<correction proftpd-dfsg 				"Retter CRL-problemer [CVE-2019-19270 CVE-2019-19269]">
<correction pykaraoke 					"Retter sti til skrifttyper">
<correction python-evtx 				"Retter import af <q>hexdump</q>">
<correction python-internetarchive 			"Lukker fil efter at have fået en hash, hvilket forhindrer fildescriptorudmattelse">
<correction python3.7 					"Sikkerhedsrettelser [CVE-2019-9740 CVE-2019-9947 CVE-2019-9948 CVE-2019-10160 CVE-2019-16056 CVE-2019-16935]">
<correction qtbase-opensource-src 			"Tilføjer understøttelse af ikke-PPD-printere og undgår tavst fallback til en printer som understøtter PPD; retter nedbrud når QLabels anvendes med rich text; retter grafiktablethoverevents">
<correction qtwebengine-opensource-src 			"Retter PDF-fortolkning; deaktiverer udførbar stak">
<correction quassel 					"Retter quasselcore AppArmor-afvisninger når opsætningen gemmes; korrigerer standardkanal i Debian; fjerner unødvendig NEWS-fil">
<correction qwinff 					"Retter nedbrud på grund af ukorrekt filgenkendelse">
<correction raspi3-firmware 				"Retter genkendelse af seriel konsol med kerne 5.x">
<correction ros-ros-comm 				"Retter sikkerhedsproblemer [CVE-2019-13566 CVE-2019-13465 CVE-2019-13445]">
<correction roundcube 					"Ny stabil opstrømsudgave; retter usikre rettigheder i enigma-plugin [CVE-2018-1000071]">
<correction schleuder 					"Retter genkendte nøgleord i mails med <q>protected headers</q> og et tomt emne; fjerner ikke-egne signaturer når der opfriskes eller hentes nøgler; fejl hvis parameteret leveret til <q>refresh_keys</q> ikke er en eksisterende liste; tilføjer manglende List-Id-header til notifikationsmails der sendes til administratorer; håndterer dekrypteringsproblemer på elegant vis; gør ASCII-8BIT til standardencoding">
<correction simplesamlphp 				"Retter inkompatiblitet med PHP 7.3">
<correction sogo-connector 				"Ny opstrømsudgave kompatibel med Thunderbird 68">
<correction spf-engine 					"Retter rettighedshåndtering ved start så Unix-sockets fungerer; opdaterer dokumentation af TestOnly">
<correction sudo 					"Retter et (ikke-udnytbart i buster) bufferoverløb når pwfeedback er aktiveret og input ikke er en tty [CVE-2019-18634]">
<correction systemd 					"Opsætter sysctl'en fs.file-max til LONG_MAX frem for ULONG_MAX; ændrer også for statiske brugere ejerskab/tilstand af udførelsesmapperne, hvilket sikrer at udførelsesmapper så som CacheDirectory og StateDirectory har en korrekt chown til brugeren angivet i User= før servicen startes">
<correction tifffile 					"Retter wrapperskript">
<correction tigervnc 					"Sikkerhedsrettelser [CVE-2019-15691 CVE-2019-15692 CVE-2019-15693 CVE-2019-15694 CVE-2019-15695]">
<correction tightvnc 					"Sikkerhedsrettelser [CVE-2014-6053 CVE-2019-8287 CVE-2018-20021 CVE-2018-20022 CVE-2018-20748 CVE-2018-7225 CVE-2019-15678 CVE-2019-15679 CVE-2019-15680 CVE-2019-15681]">
<correction uif 					"Retter stier til ip(6)tables-restore set i lyset af migreringen til nftables">
<correction unhide 					"Retter stakudmattelse">
<correction x2goclient 					"Fjerner ~/, ~user{,/}, ${HOME}{,/} og $HOME{,/} fra destinationsstier i SCP-tilstand; retter regression med nyere versioner af libssh med anvendte rettelser af CVE-2019-14889">
<correction xmltooling 					"Retter kapløbstilstand som kunne føre til nedbrud under belastning">
</table>


<h2>Sikkerhedsopdateringer</h2>

<p>Denne revision tilføjer følgende sikkerhedsopdateringer til den stabile 
udgave.  Sikkerhedsteamet har allerede udgivet bulletiner for hver af de nævnte
opdateringer:</p>

<table border=0>
<tr><th>Bulletin-id</th>  <th>Pakke(r)</th></tr>
<dsa 2019 4546 openjdk-11>
<dsa 2019 4563 webkit2gtk>
<dsa 2019 4564 linux>
<dsa 2019 4564 linux-signed-i386>
<dsa 2019 4564 linux-signed-arm64>
<dsa 2019 4564 linux-signed-amd64>
<dsa 2019 4565 intel-microcode>
<dsa 2019 4566 qemu>
<dsa 2019 4567 dpdk>
<dsa 2019 4568 postgresql-common>
<dsa 2019 4569 ghostscript>
<dsa 2019 4570 mosquitto>
<dsa 2019 4571 enigmail>
<dsa 2019 4571 thunderbird>
<dsa 2019 4572 slurm-llnl>
<dsa 2019 4573 symfony>
<dsa 2019 4575 chromium>
<dsa 2019 4577 haproxy>
<dsa 2019 4578 libvpx>
<dsa 2019 4579 nss>
<dsa 2019 4580 firefox-esr>
<dsa 2019 4581 git>
<dsa 2019 4582 davical>
<dsa 2019 4583 spip>
<dsa 2019 4584 spamassassin>
<dsa 2019 4585 thunderbird>
<dsa 2019 4586 ruby2.5>
<dsa 2019 4588 python-ecdsa>
<dsa 2019 4589 debian-edu-config>
<dsa 2019 4590 cyrus-imapd>
<dsa 2019 4591 cyrus-sasl2>
<dsa 2019 4592 mediawiki>
<dsa 2019 4593 freeimage>
<dsa 2019 4595 debian-lan-config>
<dsa 2020 4597 netty>
<dsa 2020 4598 python-django>
<dsa 2020 4599 wordpress>
<dsa 2020 4600 firefox-esr>
<dsa 2020 4601 ldm>
<dsa 2020 4602 xen>
<dsa 2020 4603 thunderbird>
<dsa 2020 4604 cacti>
<dsa 2020 4605 openjdk-11>
<dsa 2020 4606 chromium>
<dsa 2020 4607 openconnect>
<dsa 2020 4608 tiff>
<dsa 2020 4609 python-apt>
<dsa 2020 4610 webkit2gtk>
<dsa 2020 4611 opensmtpd>
<dsa 2020 4612 prosody-modules>
<dsa 2020 4613 libidn2>
<dsa 2020 4615 spamassassin>
</table>


<h2>Fjernede pakker</h2>

<p>Følgende pakker er blevet fjernet på grund af omstændigheder uden for vores 
kontrol:</p>

<table border=0>
<tr><th>Pakke</th>		<th>Årsag</th></tr>
<correction caml-crush 		"[armel] Kan ikke opbygges på grund af manglende ocaml-native-compilers">
<correction firetray 		"Inkompatibel med aktuelle versioner af Thunderbird">
<correction koji 		"Sikkerhedsproblemer">
<correction python-lamson 	"Defekt på grund af ændringer i python-daemon">
<correction radare2 		"Sikkerhedsproblemer; opstrøm tilbyder ikke support af stabil version">
<correction radare2-cutter 	"Afhængig af radare2, der vil blive fjernet">
</table>


<h2>Debian Installer</h2>

Installeringsprogrammet er opdateret for at medtage rettelser indført i stable, 
i denne punktopdatering.


<h2>URL'er</h2>

<p>Den komplette liste over pakker, som er ændret i forbindelse med denne 
revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuelle stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Foreslåede opdateringer til den stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Oplysninger om den stabile distribution (udgivelsesbemærkninger, fejl, 
osv.):</p>

<div class="center">
  <a href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sikkerhedsannonceringer og -oplysninger:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Om Debian</h2>

<p>Debian-projektet er en organisation af fri software-udviklere som frivilligt
bidrager med tid og kræfter, til at fremstille det helt frie styresystem Debian
GNU/Linux.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg Debians websider på 
<a href="$(HOME)/">https://www.debian.org/</a> eller send e-mail på engelsk til
&lt;press@debian.org&gt; eller kontakt holdet bag den stabile udgave på 
&lt;debian-release@debian.org&gt;.</p>
