#use wml::debian::translation-check translation="92233062b10a767a6a4fc85a6e524d91dbe64868" maintainer="Jean-Pierre Giraud"
#use wml::debian::template title="Domaine debian.community"
#use wml::debian::faqs

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<p style="display: block; border: solid; padding: 1em; background-color: #FFF29F;">
 <i class="fa fa-unlink fa-3x"></i>
 Si vous arrivez sur cette page au moyen d'un lien vers <a
 href="https://debian.community">debian.community</a>, la page que vous demandez
 n'existe plus. Pour savoir pourquoi, lisez ce qui suit.
</p>

<ul class="toc">
 <li><a href="#background">Contexte</a></li>
 <li><a href="#statements">Déclarations précédentes</a></li>
 <li><a href="#faq">Questions fréquemment posées</a></li>
</ul>

<h2><a id="background">Contexte</a></h2>

<p>
En juillet 2022, l'Organisation mondiale de la propriété intellectuelle (OMPI)
a déterminé que le domaine <a href="https://debian.community/">debian.community</a>
a été enregistré de mauvaise foi et a été utilisé pour ternir la marque du
Projet Debian. Elle a ordonné que le domaine soit géré par le Projet.
</p>

<h2><a id="statements">Déclarations précédentes</a></h2>

<h3><a id="statement-debian">
 Debian Project : harcèlement par Daniel Pocock
</a></h3>

<p>
 <i>Publication initiale : <a href="$(HOME)/News/2021/20211117">17 novembre 2021</a></i>
</p>

<p>
Debian a connaissance d'un certain nombre de messages publics concernant
Debian et des membres de sa communauté dans une série de sites internet par
M. Daniel Pocock qui prétend être un développeur Debian.
</p>

<p>
M. Pocock n'est pas lié à Debian. Il n'est ni développeur Debian, ni un
membre de la communauté Debian. Il a été auparavant un développeur Debian,
mais a été exclu du projet il y a quelques années pour avoir adopté un
comportement nuisible pour la réputation de Debian et pour la communauté
elle-même. Il n'est plus membre du projet Debian depuis 2018. Il lui est aussi
interdit de participer à la communauté sous quelque forme que ce soit, y
compris par des contributions techniques, la participation à des espaces en
ligne ou la participation à des conférences ou des événements. Il n'a aucun
droit ou statut pour représenter Debian en quelque qualité que ce soit, ou
pour se présenter lui-même comme développeur Debian ou membre de la communauté
Debian.
</p>

<p>
Depuis qu'il a été exclu du projet, M. Pocock s'est engagé dans une
campagne permanente et généralisée de harcèlement en représailles en envoyant
de nombreux messages en ligne incendiaires et diffamatoires, en particulier sur
un site internet qui prétend être un site Debian. Le contenu de ces messages
implique non seulement Debian, mais aussi un certain nombre de ses développeurs
et bénévoles. Il a aussi continué à se présenter frauduleusement comme un
membre de la communauté Debian dans beaucoup de ses communications et
présentations publiques.

Veuillez consulter <a href="https://bits.debian.org/2020/03/official-communication-channels.html">cet article</a>
pour prendre connaissance de la liste des canaux de communication officiels de
Debian.

Une action légale est envisagée pour, entre autres choses, diffamation,
mensonges malveillants et harcèlement.
</p>

<p>
Debian est unie en tant que communauté et contre le harcèlement. Nous avons
un code de conduite qui nous guide pour répondre aux comportements nuisibles
dans notre communauté, et nous continuerons à agir pour protéger notre
communauté et nos bénévoles. N'hésitez pas à contacter l'équipe en charge de la
communauté si vous avez des soucis ou si vous avez besoin de soutien. En
attendant, tous les droits de Debian et de ses bénévoles sont réservés.
</p>

<h3><a id="statement-other">Déclarations d'autres projets</a></h3>

<ul>
 <li>
<a href="https://fsfe.org/about/legal/minutes/minutes-2019-10-12.en.pdf#page=17">\
Association Free Software Foundation Europe</a> (publication initiale le 12 octobre 2019)
 </li>
 <li>
<a href="https://openlabs.cc/en/statement-we-have-been-a-target-of-disinformation-efforts-our-initial-reaction/">\
Open Labs</a> (publication initiale le 26 mai 2021)
 </li>
 <li>
  <a href="https://communityblog.fedoraproject.org/statement-on-we-make-fedora/">\
Fedora</a> (publication initiale le 31 janvier 2022)
 </li>
</ul>

<h2><a id="faq">Questions fréquemment posées</a></h2>

<question>
Pourquoi le domaine a-t-il été transféré à Debian ?
</question>

<answer><p>
Debian s'est plaint auprès de l'Organisation mondiale de la propriété
intellectuelle (OMPI) parce que le domaine a servi à l'utilisation
abusive de mauvaise foi des marques déposées de Debian. En juillet 2022, le
comité de l'OMPI a convenu que le déclarant précédent n'avait pas de droit ni
d'intérêts sur les marques et les utilisait de mauvaise foi, et a transféré le
domaine au Projet Debian.
</p></answer>

<question>
Quelles étaient les objections de Debian sur le contenu ?
</question>
 
<answer><p>
Le contenu du site <a href="https://debian.community/">debian.community</a>
ternissait les marques de Debian en les associant à des déclarations non fondées
et des liens vers des sectes, des insinuations d'asservissement et d'abus à
l'encontre des bénévoles.
</p></answer>

<question>
Qui était derrière le site précédent ?
</question>

<answer><p>
Le déclarant précédent du domaine était l'Association des Free Software
Contributors, association non enregistrée en Suisse. Dans sa plainte auprès
de l'OMPI, Debian a affirmé que l'association était un faux-nez de Daniel
Pocock.
</p></answer>

<question>
Peut-on consulter le jugement de l'OMPI dans son intégralité ?
</question>

<answer><p>
Oui, il est
 <a href="https://www.wipo.int/amc/en/domains/decisions/pdf/2022/d2022-1524.pdf">
archivé publiquement</a>.
</p></answer>

<question>
Le comité a-t-il constaté que les articles étaient diffamatoires ?
</question>

<answer><p>
Le comité n'est pas autorisé à faire de conclusions dans un sens ou un autre
– c'est au-delà de la compétence du comité. Il était seulement en mesure de se
prononcer sur le fait que le déclarant a enregistré et utilisé le domaine de
mauvaise foi pour ternir les marques de Debian.
</p></answer>

<question>
Debian entreprendra-t-elle d'autres actions ?
</question>

<answer><p>
Cette information ne peut pas être publiquement révélée. Le Projet Debian
continue à contrôler la situation et consulte son conseiller juridique.
</p></answer>

<h2><a id="further-info">Pour plus d'informations<a/></h2>

<ul>
 <li>
  <a href="$(HOME)/intro/people">
   Projet Debian : Les acteurs : Qui sommes-nous et que faisons-nous</a>
 </li>
 <li>
  <a href="$(HOME)/intro/philosophy">
   Projet Debian : Notre philosophie : Pourquoi et comment le faisons-nous</a>
 </li>
</ul>

