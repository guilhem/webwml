#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7885">CVE-2017-7885</a>

<p>jbig2dec 0.13 d’Artifex possède une lecture hors limites de tampon basé sur le
tas conduisant à un déni de service (plantage d'application) ou une divulgation
d’informations sensibles de la mémoire du processus, due à un dépassement
d'entier dans la fonction jbig2_decode_symbol_dict dans jbig2_symbol_dict.c
dans libjbig2dec.a lors d’une opération sur un fichier .jb2 contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7975">CVE-2017-7975</a>

<p>jbig2dec 0.13 d’Artifex, tel qu’utilisé dans Ghostscript, permet une écriture
hors limites due à un dépassement d'entier dans la fonction
jbig2_build_huffman_table dans jbig2_huffman.c lors d’opérations sur un fichier
JBIG2 contrefait, conduisant à un déni de service (plantage d'application) ou
éventuellement une exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7976">CVE-2017-7976</a>

<p>jbig2dec 0.13 d’Artifex permet une écriture et lecture hors limites due à un
dépassement d'entier dans la fonction jbig2_image_compose dans jbig2_image.c
lors d’opérations sur un fichier .jb2 contrefait, conduisant à un déni de
service (plantage d'application) ou une divulgation d’informations sensibles
de la mémoire du processus.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.13-4~deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets jbig2dec.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-942.data"
# $Id: $
