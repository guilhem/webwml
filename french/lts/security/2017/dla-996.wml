#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le mécanisme de page d’erreur de la spécification Java Servlet requiert que,
quand une erreur se produit et qu’une page d’erreur est configurée pour l’erreur
produite, la requête et la réponse originales sont transmises à la page d’erreur.
Cela signifie que la requête est présentée à la page d’erreur avec la méthode
HTTP originale. Si la page d’erreur est un fichier statique, le comportement
attendu est de servir le contenu du fichier comme le traitement d’une requête GET,
quelque soit la méthode HTTP réelle. Le Servlet par défaut dans Apache Tomcat
ne le réalisait pas. Selon la requête originale, cela pourrait conduire à des
résultats inattendus et indésirables pour les pages d’erreur statiques incluant,
si le Servlet par défaut est configuré pour autoriser les écritures, le
remplacement ou la suppression de la page d’erreur personnalisée.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 7.0.28-4+deb7u14.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat7.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-996.data"
# $Id: $
