#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans CUPS, le système commun
d'impression pour UNIX. Ces problèmes ont reçu les identifiants CVE suivants :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4180">CVE-2018-4180</a>

<p>Dan Bastone de Gotham Digital Science a découvert qu'un attaquant local
doté d'un accès à cupsctl pourrait augmenter ses droits en réglant une
variable d'environnement.</p></li>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-4181">CVE-2018-4181</a>

<p>Eric Rafaloff et John Dunlap de Gotham Digital Science ont découvert
qu'un attaquant local peut réaliser des lectures limitées de fichiers
arbitraires en tant que superutilisateur en manipulant cupsd.conf.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6553">CVE-2018-6553</a>

<p>Dan Bastone de Gotham Digital Science a découvert qu'un attaquant peut
contourner le bac à sable AppArmor de cupsd en invoquant le dorsal dnssd en
utilisant un nom différent lié directement à dnssd.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.7.5-11+deb8u4.</p>
<p>Nous vous recommandons de mettre à jour vos paquets cups.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1426.data"
# $Id: $
