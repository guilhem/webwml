#use wml::debian::translation-check translation="c9b52b4dd138ace01295f186aeb9f9845bd7869c" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème d’injection de code dans
PyXDG, une bibliothèque utilisée pour localiser les répertoires
configuration/cache/etc de freedesktop.org.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12761">CVE-2019-12761</a>

<p>Un problème d’injection de code a été découvert dans PyXDG avant la
version 0.26 à l’aide de code contrefait en Python dans un élément Category
d’un document XML Menu dans un fichier .menu. XDG_CONFIG_DIRS peut être réglé
pour déclencher une analyse xdg.Menu.parse dans le répertoire contenant
ce fichier. Cela est dû à un manque de nettoyage dans xdg/Menu.py avant
un appel à eval().</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 0.25-4+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pyxdg.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2727.data"
# $Id: $
