#use wml::debian::translation-check translation="1c2803607d4643a698f435f8675d262fb7e0aea0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs défauts ont été découverts dans HTCondor, un système distribué
de gestion de charge de travail, qui permettent à des utilisateurs dotés
d'un accès en lecture seule à tous les démons d'utiliser une méthode
d'authentification différente de celle spécifiée par l'administrateur. Si
l'administrateur a configuré les méthodes <q>READ</q> ou <q>WRITE</q> pour
inclure <q>CLAIMTOBE</q>, il est alors possible d'usurper l'identité d'un
autre utilisateur et de soumettre ou supprimer des tâches.</p>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 8.6.8~dfsg.1-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets condor.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de condor, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/condor">\
https://security-tracker.debian.org/tracker/condor</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5144.data"
# $Id: $
