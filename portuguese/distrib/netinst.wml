#use wml::debian::template title="Instalando o Debian via Internet" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="f917b9adf4a1c15cca8405e010043d380e4b1b83"

<p>Este método de instalação do Debian requer uma conexão permanente
à Internet <em>durante</em> a instalação. Comparado com outros métodos
você acaba baixando menos dados já que o processo será adaptado às suas
necessidades. Conexões de Ethernet e sem fio são suportadas. Cartões
ISDN internos infelizmente <em>não</em> são suportados.</p>
<p>Há três opções para instalações através da rede:</p>

<toc-display />
<div class="line">
<div class="item col50">

<toc-add-entry name="smallcd">CDs pequenos ou pendrives USB</toc-add-entry>

<p>A seguir estão listados arquivos de imagens.
Escolha abaixo a arquitetura do seu processador.</p>

<stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<p>Para mais informações, por favor veja: <a href="../CD/netinst/">Instalação
pela rede a partir de um CD mínimo</a></p>

<div class="line">
<div class="item col50">

<toc-add-entry name="verysmall">CDs minúsculos, pendrives USB, etc.</toc-add-entry>

<p>Você pode baixar um par de arquivos de imagens de tamanho pequeno, ideais
para pendrives USB e dispositivos similares, gravá-los na mídia e então começar
a inicialização a partir dela.</p>

<p>Há alguma diversidade no suporte à instalação a partir de várias imagens
muito pequenas entre as arquiteturas.
</p>

<p>Para mais detalhes, por favor consulte o
<a href="$(HOME)/releases/stable/installmanual">manual de instalação para a
sua arquitetura </a>, especialmente o capítulo
<q>Obtendo a mídia de instalação do sistema</q>.</p>

<p>
Aqui estão os links para os arquivos de imagens disponíveis (veja o arquivo
MANIFEST para informações):
</p>

<stable-verysmall-images />
</div>
<div class="item col50 lastcol">

<toc-add-entry name="netboot">Inicialização de rede</toc-add-entry>

<p>Você configura um servidor TFTP e um DHCP (ou BOOTP, ou RARP) que irá
fornecer a mídia de instalação para as máquinas da sua rede local.
Se a BIOS da sua máquina cliente suportar, você pode então inicializar o
sistema de instalação Debian a partir da rede (usando PXE e TFTP) e
prosseguir com a instalação do restante do Debian a partir da rede.</p>

<p>Nem todas as máquinas suportam inicialização a partir da rede. Por causa
do trabalho adicional requerido, este método de instalação do Debian não
é recomendado para usuários(as) sem experiência.</p>

<p>Para detalhes, por favor consulte o
<a href="$(HOME)/releases/stable/installmanual">manual de instalação para
sua arquitetura</a>, especialmente o capítulo
<q>Preparando arquivos para inicialização TFTP pela rede</q>.</p>
<p>Aqui estão os links para os arquivos de imagens (veja o arquivo MANIFEST
para informações):</p>

<stable-netboot-images />
</div>
</div>

# Tradutores: o seguinte parágrafo existe (nesta ou em uma forma semelhante) várias vezes nos webwml,
# portanto, tente manter as traduções consistentes. Veja:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
Se algum hardware em seu sistema <strong>requer que firmware não livre seja
carregado</strong> com o controlador do dispositivo, você pode usar um dos
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
arquivos tarball de pacotes de firmware comuns</a> ou baixar uma imagem
<strong>não oficial</strong> que inclui esses firmwares <strong>não livres</strong>.
Instruções de como usar os arquivos tarball e informações gerais sobre como
carregar um firmware durante uma instalação podem ser encontradas no
<a href="../releases/stable/amd64/ch06s04">guia de instalação</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">imagens
de instalação não oficiais para <q>estável (stable)</q> com firmwares
incluídos</a>
</p>
</div>
