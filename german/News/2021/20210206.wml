<define-tag pagetitle>Debian 10 aktualisiert: 10.8 veröffentlicht</define-tag>
<define-tag release_date>2021-02-06</define-tag>
#use wml::debian::news
# $Id:
#use wml::debian::translation-check translation="156615cc19b61bffcfb93a5ff5e5e300fcbc9492" maintainer="Erik Pfannenstein"

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die achte Aktualisierung für seine stabile 
Distribution Debian <release> (codename <q><codename></q>) ankündigen zu dürfen. 
Diese Zwischenveröffentlichung bringt hauptsächlich Korrekturen für 
Sicherheitsprobleme und einige Anpassungen für einige ernste Probleme. Für 
sie sind bereits separate Sicherheitsankündigungen veröffentlicht worden; auf 
diese wird, wo vorhanden, verwiesen.
</p>

<p>
Bitte beachten Sie, dass diese Zwischenveröffentlichung keine neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete 
auffrischt. Es gibt keinen Grund, Buster-Medien zu entsorgen, da deren Pakete 
nach der Installation mit Hilfe eines aktuellen Debian-Spiegelservers auf den 
neuesten Stand gebracht werden können. 
</p>

<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird nicht
viele Pakete auf den neuesten Stand bringen müssen. Die meisten
Aktualisierungen sind in dieser Revision enthalten.
</p>

<p>Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden.</p>

<p>
Vorhandene Installationen können auf diese Revision angehoben werden, indem das
Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian verwiesen
wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter: 
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>
<p>
Diese Stable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige Korrekturen vor:
</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction atftp "Dienstblockadeproblem behoben [CVE-2020-6097]">
<correction base-files "/etc/debian_version auf die Version 10.8 aktualisiert">
<correction ca-certificates "Mozilla-CA-Bündel auf 2.40 aktualisiert, abgelaufenes <q>AddTrust External Root</q> gesperrt">
<correction cacti "Probleme mit SQL-Injektion [CVE-2020-35701] und stored XSS behoben">
<correction cairo "Maskenverwendung im image-compositor überarbeitet [CVE-2020-35492]">
<correction choose-mirror "Liste der Spiegel aktualisiert">
<correction cjson "Endlosschleife in cJSON_Minify behoben">
<correction clevis "Initramfs-Erstellung überarbeitet; clevis-dracut: Initramfs-Erstellung während Installation auslösen">
<correction cyrus-imapd "Versionsvergleich im Cron-Skript korrigiert">
<correction debian-edu-config "Aufräum-Code für Host-Keytabs aus gosa-modify-host in ein eigenständiges Skript übertragen, um die LDAP-Aufrufe auf eine einzelne Abfrage zu reduzieren">
<correction debian-installer "Linux-Kernel-ABI 4.19.0-14 verwenden; Neukompilierung gegen proposed-updates">
<correction debian-installer-netboot-images "Neukompilierung gegen proposed-updates">
<correction debian-installer-utils "Partitionen auf USB-UAS-Geräten unterstützen">
<correction device-tree-compiler "Speicherzugriffsfehler auf <q>dtc -I fs /proc/device-tree</q> behoben">
<correction didjvu "Fehlende Kompilierungsabhängigkeit von tzdata nachgetragen">
<correction dovecot "Absturz beim Durchsuchen von Postfächern, die fehlerhafte MIME-Nachrichten enthalten, behoben">
<correction dpdk "Neue stabile Version der Originalautoren">
<correction edk2 "CryptoPkg/BaseCryptLib: NULL-Dereferenzierung behoben [CVE-2019-14584]">
<correction emacs "Nicht abstürzen, wenn OpenPGP-Benutzer-IDs keine E-Mail-Adresse haben">
<correction fcitx "Unterstützung für Eingabemethoden in Flatpaks überarbeitet">
<correction file "Vorgabe für Namen-Rekursionstiefe auf 50 angehoben">
<correction geoclue-2.0 "Höchstens erlaubte Genauigkeitsstufe auch für Systemanwendungen überprüfen; Mozilla-API-Schlüssel konfigurierbar machen und standardmäßig einen Debian-spezifischen Schlüssel verwenden; Darstellung der Vewendungsanzeige korrigiert">
<correction gnutls28 "Test-Suiten-Fehler behoben, der durch abgelaufene Zertifikate verursacht wurde">
<correction grub2 "Beim nicht-interaktiven Upgrade von grub-pc aussteigen, wenn grub-install fehlschlägt; explizit überprüfen, ob das Zielgerät existiert, bevor grub-install ausgeführt wird; grub-install: Datensicherung und -wiederherstellung hinzugefügt; grub-install auf einer frischen Installation von grub-pc nicht aufrufen">
<correction highlight.js "Prototype Pollution behoben [CVE-2020-26237]">
<correction intel-microcode "Verschiedene Mikrocodes aktualisiert">
<correction iproute2 "Fehler in der JSON-Ausgabe behoben; Race Condition behoben, die das System blockiert, wenn beim Hochfahren ip netns add verwendet wird">
<correction irssi-plugin-xmpp "Die Zeitüberschreitung in irssi core connect nicht vorzeitig auslösen, um Probleme mit STARTTLS-Verbindungen zu beheben">
<correction libdatetime-timezone-perl "Aktualisierung auf die neue tzdata-Version">
<correction libdbd-csv-perl "Testfehlschlag mit libdbi-perl 1.642-1+deb10u2 behoben">
<correction libdbi-perl "Sicherheitskorrektur [CVE-2014-10402]">
<correction libmaxminddb "Heap-basiertes übermäßiges Lesen des Puffers (buffer over-read) behoben [CVE-2020-28241]">
<correction lttng-modules "Kompilierungsprobleme bei Kernel-Versionen &gt;= 4.19.0-10 behoben">
<correction m2crypto "Kompatibilität mit OpenSSL 1.1.1i und neuer verbessert">
<correction mini-buildd "builder.py: sbuild-Aufruf: '--no-arch-all' explizit setzen">
<correction net-snmp "snmpd: cacheTime- und execType-Schalter zu EXTEND-MIB hinzufügen">
<correction node-ini "Keine ungültige gefährliche Zeichenkette als Sektionsnamen erlauben [CVE-2020-7788]">
<correction node-y18n "Problem mit Prototype Pollution behoben [CVE-2020-7774]">
<correction nvidia-graphics-drivers "Neue Version der Originalautoren; mögliche Dienstblockade und Informationsoffenlegung behoben [CVE-2021-1056]">
<correction nvidia-graphics-drivers-legacy-390xx "Neue Version der Originalautoren; mögliche Dienstblockade und Informationsoffenlegung behoben [CVE-2021-1056]">
<correction pdns "Sicherheitskorrekturen [CVE-2019-10203 CVE-2020-17482]">
<correction pepperflashplugin-nonfree "In Pseudo-Paket umgewandelt, welches sich um die Entfernung des installierten Plugins kümmert (funktioniert nicht mehr und wird nicht mehr untestützt)">
<correction pngcheck "Pufferüberlauf behoben [CVE-2020-27818]">
<correction postgresql-11 "Neue stabile Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2020-25694 CVE-2020-25695 CVE-2020-25696]">
<correction postsrsd "Sicherstellen, dass Zeitstempel-Tags nicht zu lang sind, bevor versucht wird, sie zu decodieren [CVE-2020-35573]">
<correction python-bottle "Aufhören <q>;</q> als Trennzeichen für Abfragezeichenketten zu erlauben [CVE-2020-28473]">
<correction python-certbot "Automatisch ACMEv2-API zum Erneuern benutzen, um Probleme mit der Entfernung der ACMEv1-API zu vermeiden">
<correction qxmpp "Potenziellen Speicherzugriffsfehler bei Verbindungsfehlern behoben">
<correction silx "python(3)-silx: Abhängigkeit von python(3)-scipy hinzugefügt">
<correction slirp "Pufferüberläufe behoben [CVE-2020-7039 CVE-2020-8608]">
<correction steam "Neue Version der Originalautoren">
<correction systemd "journal: Assertion nicht auslösen, wenn journal_file_close() ein NULL übergeben wird">
<correction tang "Race Condition zwischen keygen und update vermeiden">
<correction tzdata "Neue Veröffentlichung der Originalautoren; enthaltene Zeitzonendaten aktualisiert">
<correction unzip "Weitere Korrekturen für CVE-2019-13232">
<correction wireshark "Verschiedene Abstürze, Endlosschleifen und Speicherlecks behoben [CVE-2019-16319 CVE-2019-19553 CVE-2020-11647 CVE-2020-13164 CVE-2020-15466 CVE-2020-25862 CVE-2020-25863 CVE-2020-26418 CVE-2020-26421 CVE-2020-26575 CVE-2020-28030 CVE-2020-7045 CVE-2020-9428 CVE-2020-9430 CVE-2020-9431]">
</table>

<h2>Sicherheitsaktualisierungen</h2>

<p>
Diese Revision fügt der Stable-Veröffentlichung die folgenden
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für jede
davon eine Ankündigung veröffentlicht:
</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2020 4797 webkit2gtk>
<dsa 2020 4801 brotli>
<dsa 2020 4802 thunderbird>
<dsa 2020 4803 xorg-server>
<dsa 2020 4804 xen>
<dsa 2020 4805 trafficserver>
<dsa 2020 4806 minidlna>
<dsa 2020 4807 openssl>
<dsa 2020 4808 apt>
<dsa 2020 4809 python-apt>
<dsa 2020 4810 lxml>
<dsa 2020 4811 libxstream-java>
<dsa 2020 4812 xen>
<dsa 2020 4813 firefox-esr>
<dsa 2020 4814 xerces-c>
<dsa 2020 4815 thunderbird>
<dsa 2020 4816 mediawiki>
<dsa 2020 4817 php-pear>
<dsa 2020 4818 sympa>
<dsa 2020 4819 kitty>
<dsa 2020 4820 horizon>
<dsa 2020 4821 roundcube>
<dsa 2021 4822 p11-kit>
<dsa 2021 4823 influxdb>
<dsa 2021 4824 chromium>
<dsa 2021 4825 dovecot>
<dsa 2021 4827 firefox-esr>
<dsa 2021 4828 libxstream-java>
<dsa 2021 4829 coturn>
<dsa 2021 4830 flatpak>
<dsa 2021 4831 ruby-redcarpet>
<dsa 2021 4832 chromium>
<dsa 2021 4833 gst-plugins-bad1.0>
<dsa 2021 4834 vlc>
<dsa 2021 4835 tomcat9>
<dsa 2021 4837 salt>
<dsa 2021 4838 mutt>
<dsa 2021 4839 sudo>
<dsa 2021 4840 firefox-esr>
<dsa 2021 4841 slurm-llnl>
<dsa 2021 4843 linux-latest>
<dsa 2021 4843 linux-signed-amd64>
<dsa 2021 4843 linux-signed-arm64>
<dsa 2021 4843 linux-signed-i386>
<dsa 2021 4843 linux>
</table>

<h2>Entfernte Pakete</h2>

<p>Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb unserer Kontrolle liegen:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction compactheader "Inkompatibel mit aktuellen Thunderbird-Versionen">
</table>

<h2>Debian-Installer</h2>

<p>Der Installer wurde aktualisiert, damit er die Sicherheitskorrekturen enthält,
die durch diese Zwischenveröffentlichung in Stable eingeflossen sind.</p>

<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert haben:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Veröffentlichung:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier Software, 
die ihre Zeit und Bemühungen einbringen, um das vollständig freie 
Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail (auf 
Englisch) an &lt;press@debian.org&gt;, oder kontaktieren das 
Stable-Release-Team (auch auf Englisch) über 
&lt;debian-release@lists.debian.org&gt;.</p>
