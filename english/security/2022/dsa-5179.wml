<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Charles Fol discovered two security issues in PHP, a widely-used open
source general purpose scripting language which could result an denial of
service or potentially the execution of arbitrary code:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31625">CVE-2022-31625</a>

    <p>Incorrect memory handling in the pg_query_params() function.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31626">CVE-2022-31626</a>

    <p>A buffer overflow in the mysqld extension.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 7.4.30-1+deb11u1.</p>

<p>We recommend that you upgrade your php7.4 packages.</p>

<p>For the detailed security status of php7.4 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/php7.4">\
https://security-tracker.debian.org/tracker/php7.4</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5179.data"
# $Id: $
