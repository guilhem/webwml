<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that phpseclib, a pure-PHP implementation of various
cryptographic and arithmetic algorithms (v1), mishandles RSA PKCS#1
v1.5 signature verification. An attacker may get invalid signatures
accepted, bypassing authorization control in specific situations.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.0.19-3~deb10u1.</p>

<p>We recommend that you upgrade your phpseclib packages.</p>

<p>For the detailed security status of phpseclib please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/phpseclib">https://security-tracker.debian.org/tracker/phpseclib</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3197.data"
# $Id: $
