<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the ISC DHCP client,
relay and server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2928">CVE-2022-2928</a>

    <p>It was discovered that the DHCP server does not correctly perform
    option reference counting when configured with "allow leasequery;".
    A remote attacker can take advantage of this flaw to cause a denial
    of service (daemon crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2929">CVE-2022-2929</a>

    <p>It was discovered that the DHCP server is prone to a memory leak
    flaw when handling contents of option 81 (fqdn) data received in
    a DHCP packet. A remote attacker can take advantage of this flaw
    to cause DHCP servers to consume resources, resulting in denial
    of service.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
4.4.1-2+deb10u2.</p>

<p>We recommend that you upgrade your isc-dhcp packages.</p>

<p>For the detailed security status of isc-dhcp please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/isc-dhcp">https://security-tracker.debian.org/tracker/isc-dhcp</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3146.data"
# $Id: $
