<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>jQuery-UI, the official jQuery user interface library, is a curated set
of user interface interactions, effects, widgets, and themes built on top
of jQuery were reported to have the following vulnerabilities.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41182">CVE-2021-41182</a>

    <p>jQuery-UI was accepting the value of the `altField` option of the
    Datepicker widget from untrusted sources may execute untrusted code.
    This has been fixed and now any string value passed to the `altField`
    option is now treated as a CSS selector.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41183">CVE-2021-41183</a>

    <p>jQuery-UI was accepting the value of various `*Text` options of the
    Datepicker widget from untrusted sources may execute untrusted code.
    This has been fixed and now the values passed to various `*Text`
    options are now always treated as pure text, not HTML.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41184">CVE-2021-41184</a>

    <p>jQuery-UI was accepting the value of the `of` option of the
    `.position()` util from untrusted sources may execute untrusted code.
    This has been fixed and now any string value passed to the `of`
    option is now treated as a CSS selector.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31160">CVE-2022-31160</a>

    <p>jQuery-UI was potentially vulnerable to cross-site scripting.
    Initializing a checkboxradio widget on an input enclosed within a
    label makes that parent label contents considered as the input label.
    Calling `.checkboxradio( <q>refresh</q> )` on such a widget and the initial
    HTML contained encoded HTML entities will make them erroneously get
    decoded. This can lead to potentially executing JavaScript code.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.12.1+dfsg-5+deb10u1.</p>

<p>We recommend that you upgrade your jqueryui packages.</p>

<p>For the detailed security status of jqueryui please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/jqueryui">https://security-tracker.debian.org/tracker/jqueryui</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3230.data"
# $Id: $
