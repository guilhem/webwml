<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in Cacti, a web interface for
graphing of monitoring systems, leading to authentication bypass and
cross-site scripting (XSS). An attacker may get access to unauthorized
areas and impersonate other users, under certain conditions.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10060">CVE-2018-10060</a>

    <p>Cacti has XSS because it does not properly reject unintended
    characters, related to use of the sanitize_uri function in
    lib/functions.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10061">CVE-2018-10061</a>

    <p>Cacti has XSS because it makes certain htmlspecialchars calls
    without the ENT_QUOTES flag (these calls occur when the
    html_escape function in lib/html.php is not used).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11025">CVE-2019-11025</a>

    <p>No escaping occurs before printing out the value of the SNMP
    community string (SNMP Options) in the View poller cache, leading
    to XSS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7106">CVE-2020-7106</a>

    <p>Cacti has stored XSS in multiple files as demonstrated by the
    description parameter in data_sources.php (a raw string from the
    database that is displayed by $header to trigger the XSS).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13230">CVE-2020-13230</a>

    <p>Disabling a user account does not immediately invalidate any
    permissions granted to that account (e.g., permission to view
    logs).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-23226">CVE-2020-23226</a>

    <p>Multiple Cross Site Scripting (XSS) vulnerabilities exist in
    multiple files.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23225">CVE-2021-23225</a>

    <p>Cacti allows authenticated users with User Management permissions
    to inject arbitrary web script or HTML in the <q>new_username</q> field
    during creation of a new user via <q>Copy</q> method at user_admin.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0730">CVE-2022-0730</a>

    <p>Under certain ldap conditions, Cacti authentication can be
    bypassed with certain credential types.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
0.8.8h+ds1-10+deb9u2.</p>

<p>We recommend that you upgrade your cacti packages.</p>

<p>For the detailed security status of cacti please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cacti">https://security-tracker.debian.org/tracker/cacti</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2965.data"
# $Id: $
