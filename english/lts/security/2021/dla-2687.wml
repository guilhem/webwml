<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security issues have been discovered in prosody:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32917">CVE-2021-32917</a>

    <p>The proxy65 component allows open access by default, even if neither of the
    users has an XMPP account on the local server, allowing unrestricted use of
    the server's bandwidth.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32921">CVE-2021-32921</a>

    <p>Authentication module does not use a constant-time algorithm for comparing
    certain secret strings when running under Lua 5.2 or later. This can
    potentially be used in a timing attack to reveal the contents of secret
    strings to an attacker.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
0.9.12-2+deb9u3.</p>

<p>We recommend that you upgrade your prosody packages.</p>

<p>For the detailed security status of prosody please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/prosody">https://security-tracker.debian.org/tracker/prosody</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2687.data"
# $Id: $
