<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>encoding.c in GNU Screen through 4.8.0 allows remote attackers
to cause a denial of service (invalid write access and application
crash) or possibly have unspecified other impact via a crafted
UTF-8 character sequence.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
4.5.0-6+deb9u1.</p>

<p>We recommend that you upgrade your screen packages.</p>

<p>For the detailed security status of screen please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/screen">https://security-tracker.debian.org/tracker/screen</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2570.data"
# $Id: $
