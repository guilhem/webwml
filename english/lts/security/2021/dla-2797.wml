<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update includes the changes in tzdata 2021e. Notable
changes are:</p>

<p>- Fiji suspends DST for the 2021/2022 season.
- Palestine falls back 2021-10-29 (not 2021-10-30) at 01:00.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2021a-0+deb9u2.</p>

<p>We recommend that you upgrade your tzdata packages.</p>

<p>For the detailed security status of tzdata please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tzdata">https://security-tracker.debian.org/tracker/tzdata</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2797.data"
# $Id: $
