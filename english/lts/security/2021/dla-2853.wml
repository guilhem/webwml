<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A cookie prefix spoofing vulnerability in CGI::Cookie.parse and a
regular expression denial of service vulnerability (ReDoS) on date
parsing methods was discovered in src:ruby2.1, the Ruby interpreter.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
2.3.3-1+deb9u11.</p>

<p>We recommend that you upgrade your ruby2.3 packages.</p>

<p>For the detailed security status of ruby2.3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby2.3">https://security-tracker.debian.org/tracker/ruby2.3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2853.data"
# $Id: $
