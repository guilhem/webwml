<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In XStream there is a vulnerability which may allow a remote attacker to
load and execute arbitrary code from a remote host only by manipulating the
processed input stream.</p>

<p>The type hierarchies for java.io.InputStream, java.nio.channels.Channel,
javax.activation.DataSource and javax.sql.rowsel.BaseRowSet are now
blacklisted as well as the individual types
com.sun.corba.se.impl.activation.ServerTableEntry,
com.sun.tools.javac.processing.JavacProcessingEnvironment$NameProcessIterator,
sun.awt.datatransfer.DataTransferer$IndexOrderComparator, and
sun.swing.SwingLazyValue. Additionally the internal type
Accessor$GetterSetterReflection of JAXB, the internal types
MethodGetter$PrivilegedGetter and ServiceFinder$ServiceNameIterator of
JAX-WS, all inner classes of javafx.collections.ObservableList and an
internal ClassLoader used in a private BCEL copy are now part of the
default blacklist and the deserialization of XML containing one of the
types will fail. You will have to enable these types by explicit
configuration, if you need them.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1.4.11.1-1+deb9u2.</p>

<p>We recommend that you upgrade your libxstream-java packages.</p>

<p>For the detailed security status of libxstream-java please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libxstream-java">https://security-tracker.debian.org/tracker/libxstream-java</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2616.data"
# $Id: $
