<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in gst-plugins-bad0.10, a package
containing GStreamer plugins from the <q>bad</q> set.</p>

<p>All issues are about use-after-free, out of bounds reads or buffer
overflow in different modules.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.10.23-7.4+deb8u3.</p>

<p>We recommend that you upgrade your gst-plugins-bad0.10 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2164.data"
# $Id: $
