<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in mercurial, an easy-to-use,
scalable distributed version control system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17458">CVE-2017-17458</a>

    <p>In Mercurial before 4.4.1, it is possible that a specially
    malformed repository can cause Git subrepositories to run
    arbitrary code in the form of a .git/hooks/post-update script
    checked into the repository. Typical use of Mercurial prevents
    construction of such repositories, but they can be created
    programmatically.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13346">CVE-2018-13346</a>

    <p>The mpatch_apply function in mpatch.c in Mercurial before 4.6.1
    incorrectly proceeds in cases where the fragment start is past the
    end of the original data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13347">CVE-2018-13347</a>

    <p>mpatch.c in Mercurial before 4.6.1 mishandles integer addition and
    subtraction.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13348">CVE-2018-13348</a>

    <p>The mpatch_decode function in mpatch.c in Mercurial before 4.6.1
    mishandles certain situations where there should be at least 12
    bytes remaining after the current position in the patch data, but
    actually are not.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000132">CVE-2018-1000132</a>

    <p>Mercurial version 4.5 and earlier contains a Incorrect Access
    Control (CWE-285) vulnerability in Protocol server that can result
    in Unauthorized data access. This attack appear to be exploitable
    via network connectivity. This vulnerability appears to have been
    fixed in 4.5.1.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3902">CVE-2019-3902</a>

    <p>Symbolic links and subrepositories could be used defeat Mercurial's
    path-checking logic and write files outside the repository root.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.0-1+deb9u2.</p>

<p>We recommend that you upgrade your mercurial packages.</p>

<p>For the detailed security status of mercurial please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mercurial">https://security-tracker.debian.org/tracker/mercurial</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2293.data"
# $Id: $
