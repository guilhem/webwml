<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability has been found in supervisor, a system for controlling
process state, where an authenticated client can send a malicious
XML-RPC request to supervisord that will run arbitrary shell commands
on the server. The commands will be run as the same user as supervisord.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.0a8-1.1+deb7u2.</p>

<p>We recommend that you upgrade your supervisor packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1047.data"
# $Id: $
