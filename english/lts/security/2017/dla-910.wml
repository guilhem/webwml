<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-3157">CVE-2017-3157</a>

    <p>Ben Hayak discovered that objects embedded in Writer and Calc
    documents may result in information disclosure. Please see
    <a href="https://www.libreoffice.org/about-us/security/advisories/cve-2017-3157/">https://www.libreoffice.org/about-us/security/advisories/cve-2017-3157/</a>
    for additional information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7870">CVE-2017-7870</a>

    <p>An out-of-bounds write caused by a heap-based buffer overflow was
    found in the Polygon class.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:3.5.4+dfsg2-0+deb7u9.</p>

<p>We recommend that you upgrade your libreoffice packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-910.data"
# $Id: $
